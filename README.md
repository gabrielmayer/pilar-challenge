# Pilar Challenge

## O que utilizei para o projeto

- Python + Flask para o desenvolvimento da API.
- Ward para realizar o testes.
- Flake8 para lint do código.
- Zappa para realizar o deploy do projeto e converter todos os endpoints em funções lambda que são utilizadas no próprio API Gateway da AWS.
- CI CD do próprio Gitlab para realizar todo o fluxo de lint, testes e deploy ao receber um novo commit na master.

## Justificando minhas escolhas de tecnologia

Acredito que utilizar microserviços em serverless, além de ser mais fácil, reduz muito os custos de uma infraestrutura. Além disso, quando utilizamos serviços como o API Gateway da AWS, tarefas como autoscalling e load balancing são feitas automaticamente pela própria AWS, evitando a necessidade de mais configurações conforme a demanda de uso cresce. Utilizei o Zappa para realizar o projeto justamente pela sua simplicidade para criar toda a estrutura necessária dentro da AWS para que possamos rodar nossos micro-serviços em serverless.

## Como rodar o projeto

````
git clone https://gitlab.com/gabrielmayer/pilar-challenge
cd pilar-challenge
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python3 main.py
````

## Como fazer um novo deploy

Para realizar um novo deploy, basta fazer um commit na master e o pipeline de CI CD será ativado automaticamente
