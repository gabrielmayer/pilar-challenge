from ward import test
from app.utils.helpers import count_vowels


@test("Test count_vowels with random string")
def test_count_vowels_with_random_string():

    test_string = "batman"
    vowels = count_vowels(test_string)

    assert vowels == 2


@test("Test count_vowels function with a string without vowels")
def test_count_vowels_without_vowels():

    test_string = "mlfk"
    vowels = count_vowels(test_string)

    assert vowels == 0


@test("Test count_vowels function with a string with only vowels")
def test_count_vowels_with_only_vowels():

    test_string = "aeiou"
    vowels = count_vowels(test_string)

    assert vowels == 5


@test("Test count_vowels function with a string with only numbers")
def test_count_vowels_with_numbers():

    test_string = "1324324552"
    vowels = count_vowels(test_string)

    assert vowels == 0
    
