vowels_list = ['a', 'e', 'i', 'o', 'u']


def count_vowels(word):

    vowels_counter = 0

    for letter in word:

        if letter in vowels_list:

            vowels_counter += 1

    return vowels_counter
