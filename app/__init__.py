from flask import Flask
from flask_cors import CORS
from app.exceptions import handle_bad_request
from werkzeug.exceptions import HTTPException


def create_app():

    this_app = Flask(__name__)

    this_app.config["JSON_SORT_KEYS"] = False
    this_app.register_error_handler(HTTPException, handle_bad_request)

    CORS(this_app, resources={r"/api/*": {"origins": "*"}})

    from app.pilar_routes import pilar_routes_blueprint
    this_app.register_blueprint(pilar_routes_blueprint, url_prefix="/api")

    from app.health_check import healt_check_blueprint
    this_app.register_blueprint(healt_check_blueprint, url_prefix="/")

    return this_app


app = create_app()