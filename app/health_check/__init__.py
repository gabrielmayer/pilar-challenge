from flask import Blueprint

from .view import HealthCheck

health_check_view = HealthCheck.as_view("health_check")

health_check_views = [
    ("", health_check_view, ["GET"]),
]

healt_check_blueprint = Blueprint("health_check", __name__)

for uri, view_func, methods in health_check_views:
    healt_check_blueprint.add_url_rule(
        uri, view_func=view_func, methods=methods
    )
