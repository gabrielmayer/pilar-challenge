from ward import test, fixture
from app import app
import json


@fixture(scope="global")
def test_api_client():
    with app.test_client() as client:
        yield client


@test("Test health_check endpoint")
def test_health_check_endpoint(client=test_api_client):

    expected_result = {'status': 'ok!!!'}
    expected_status_code = 200
    
    response = client.get("/")

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json == expected_result 
        and response.status_code == expected_status_code
    )