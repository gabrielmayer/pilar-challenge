from flask import Blueprint

from .view import CountVowels, SortWords

count_vowels_view = CountVowels.as_view("count_vowels")
sort_words_view = SortWords.as_view("sort_words")

pilar_routes_views = [
    ("/vowel_count", count_vowels_view, ["POST"]),
    ("/sort", sort_words_view, ["POST"])
]

pilar_routes_blueprint = Blueprint("pilar_routes", __name__)

for uri, view_func, methods in pilar_routes_views:
    pilar_routes_blueprint.add_url_rule(
        uri, view_func=view_func, methods=methods
    )