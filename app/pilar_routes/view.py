from flask.views import MethodView
from flask import request, jsonify
from app.utils.helpers import count_vowels


# @app.route('/vowel_count', methods=[POST])
class CountVowels(MethodView):

    decorators = []

    def post(self):

        data = request.get_json()

        result = {}

        for word in data.get('words'):

            result[word] = count_vowels(word)

        return result, 200


# @app.route('/sort', methods=[POST])
class SortWords(MethodView):

    decorators = []

    def post(self):

        data = request.get_json()

        words_list = data.get('words')

        if data.get('order') == 'asc':
            return jsonify(sorted(words_list)), 200

        else:
            return jsonify(sorted(words_list, reverse=True)), 200
        