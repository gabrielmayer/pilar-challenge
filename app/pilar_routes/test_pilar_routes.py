from ward import test, fixture
from app import app
import json


@fixture(scope="global")
def test_api_client():
    with app.test_client() as client:
        yield client


@test("Test /vowel_count endpoint with sample json")
def test_vowel_count_endpoint_with_sample_json(client=test_api_client):

    sample_payload = {
        "words": ["batman", "robin", "coringa"]
    }

    expected_result = {
        "batman": 2,
        "robin": 2,
        "coringa": 3
    }
    expected_status_code = 200

    response = client.post("/api/vowel_count", json=sample_payload)

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json == expected_result 
        and response.status_code == expected_status_code
    )


@test("Test /vowel_count endpoint with invalid payload")
def test_vowel_count_endpoint_with_invalid_payload(client=test_api_client):

    invalid_payload = {"word": ["batman", "robin", "coringa"]}

    expected_error_message = "The browser (or proxy) sent a request that " \
                             "this server could not understand."
    expected_status_code = 400

    response = client.post("/api/vowel_count", data=invalid_payload)

    print(response.get_data())

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json.get("description") == expected_error_message
        and response_json.get("code") == expected_status_code
    )


@test("Test /sort endpoint in asc order with sample json")
def test_sort_endpoint_asc_with_sample_json(client=test_api_client):

    sample_payload = {
        "words": ["batman", "robin", "coringa"], 
        "order": "asc"
    }

    expected_result = ["batman", "coringa", "robin"]
    expected_status_code = 200

    response = client.post("/api/sort", json=sample_payload)

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json == expected_result 
        and response.status_code == expected_status_code
    )
    

@test("Test /sort endpoint in desc order with sample json")
def test_sort_endpoint_desc_with_sample_json(client=test_api_client):

    sample_payload = {
        "words": ["batman", "robin", "coringa"], 
        "order": "desc"
    }

    expected_result = ["robin", "coringa", "batman"]
    expected_status_code = 200

    response = client.post("/api/sort", json=sample_payload)

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json == expected_result 
        and response.status_code == expected_status_code
    )


@test("Test /sort endpoint with invalid payload")
def test_sort_endpoint_with_invalid_payload(client=test_api_client):

    invalid_payload = ["batman", "robin", "coringa"]

    expected_error_message = "The browser (or proxy) sent a request that " \
                             "this server could not understand."
    expected_status_code = 400

    response = client.post("/api/sort", data=json.dumps(invalid_payload))

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json.get("description") == expected_error_message 
        and response_json.get("code") == expected_status_code
    )


@test("Test /sort endpoint without payload")
def test_sort_endpoint_without_payload(client=test_api_client):

    expected_error_message = "The browser (or proxy) sent a request that " \
                             "this server could not understand."
    expected_status_code = 400

    response = client.post("/api/sort", json=None)

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json.get("description") == expected_error_message 
        and response_json.get("code") == expected_status_code
    )


@test("Test not existing endpoint")
def test_not_existing_endpoint(client=test_api_client):

    sample_payload = {
        "words": ["batman", "robin", "coringa"]
    }

    expected_descrition = "The requested URL was not found on the server. " \
                          "If you entered the URL manually please check " \
                          "your spelling and try again."
    expected_status_code = 404

    response = client.post("/api/vowel_counter", json=sample_payload)

    response_json = json.loads(response.get_data(as_text=True))

    assert (
        response_json.get("description") == expected_descrition 
        and response_json.get("code") == expected_status_code
    )
    
